import json
import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("workstations")


def test_consumer_auth(host):
    cmd = host.run(
        "python3 -c \"import json, kafka; print(json.dumps(list(kafka.KafkaConsumer(bootstrap_servers='ics-ans-alarm-server:9092').topics())))\""
    )
    assert sorted(json.loads(cmd.stdout)) == sorted(
        ["TEST2", "TESTCommand", "TEST2Command", "TESTTalk", "TEST", "TEST2Talk"]
    )


def test_producer_auth(host):
    cmd = host.run(
        "python3 -c \"import kafka; producer = kafka.KafkaProducer(bootstrap_servers=['ics-ans-alarm-server:9092']); future = producer.send('TESTTalk', b'test-message'); record_metadata = future.get(timeout=10);\""
    )
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-alarm-server-ws":
        assert cmd.rc == 0
    else:
        assert cmd.rc != 0
        assert "TopicAuthorizationFailedError" in cmd.stderr
