---
- hosts: alarm_server
  become: true
  roles:
    - role: ics-ans-role-alarm-server
  tasks:
    - name: List Kafka cluster ACLs
      command:
        cmd: "{{ kafka_home }}/bin/kafka-acls.sh --list --bootstrap-server localhost:9092 --cluster --principal=User:*"
      changed_when: false
      register: alarm_server_kafka_acls_cluster

    - name: Allow Kafka cluster local access
      command:
        cmd: "{{ kafka_home }}/bin/kafka-acls.sh --add --bootstrap-server localhost:9092 --cluster --allow-principal=User:*{% for ip in ansible_all_ipv4_addresses %} --allow-host={{ ip }}{% endfor %}"
      when: ansible_all_ipv4_addresses | difference(alarm_server_kafka_acls_cluster.stdout_lines | select('search', 'host=[^ ]*') | map('regex_replace', '.*host=(((25[0-5]|(2[0-4]|1\\d|[1-9]|)\\d)\\.?\\b){4}).*', '\\1') | list)

    - name: Allow Kafka topic local access
      kafka_acl:
        topic: "*"
        principal: User:*
        host: "{{ item }}"
      loop: "{{ ansible_all_ipv4_addresses + [ansible_lo['ipv4']['address']] }}"

    - name: Allow Kafka topic producer access
      kafka_acl:
        topic: "*"
        principal: User:*
        host: "{{ item if item == '*' else (hostvars[item]['csentry_interfaces'] | first)['ip'] }}"
      loop: "{{ alarm_server_producer_hosts }}"

    - name: Allow Kafka topic consumer access
      kafka_acl:
        topic: "*"
        principal: User:*
        operation: read

    - name: Allow Kafka access from F04 to ACK alarms in F04 config
      kafka_acl:
        topic: "F04Command"
        principal: User:*
        host: "{{ item }}"
      when: inventory_hostname in ['alarm-03.tn.esss.lu.se', 'alarm-02.tn.esss.lu.se']
      loop:
        - "172.16.6.95"
        - "172.20.72.17"
        - "172.17.240.30"

- hosts: alarm_logger
  become: true
  tags: debug
  pre_tasks:
    - name: Install XML support for Python
      package:
        name: "{{ 'python-lxml' if ansible_os_family == 'RedHat' else 'python3-lxml' }}"
        update_cache: true
      when: hostvars[alarm_logger_kafka_host] and hostvars[alarm_logger_kafka_host].alarm_server_config_urls

    - name: Get Alarm configuration content
      uri:
        url: "{{ item }}"
        return_content: true
      loop: "{{ hostvars[alarm_logger_kafka_host].alarm_server_config_urls }}"
      when: hostvars[alarm_logger_kafka_host] and hostvars[alarm_logger_kafka_host].alarm_server_config_urls
      register: alarm_configs_xml
      tags: debug

    - name: Parse Alarm configuration name
      xml:
        xmlstring: "{{ item.content }}"
        xpath: /config
        content: attribute
      loop: "{{ alarm_configs_xml.results }}"
      when: hostvars[alarm_logger_kafka_host] and hostvars[alarm_logger_kafka_host].alarm_server_config_urls
      register: alarm_configs

    - name: Define variable for Alarm logger topics
      set_fact:
        alarm_logger_topics: "{{ alarm_configs.results | map(attribute='matches') | flatten | map(attribute='config.name') | list }}"
      when: hostvars[alarm_logger_kafka_host] and hostvars[alarm_logger_kafka_host].alarm_server_config_urls

  roles:
    - role: ics-ans-role-alarm-logger
    - role: ics-ans-role-sssd
