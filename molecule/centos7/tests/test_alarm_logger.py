import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('alarm_logger')


def test_service(host):
    alarm_server = host.service('alarm-logger')
    assert alarm_server.is_running


def test_topics_config(host):
    cmd = host.run("systemctl show alarm-logger --property=ExecStart")
    assert "-topics TEST,TEST2 " in cmd.stdout
